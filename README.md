# Vault

The vault is a simple docker-compose file that will build the ultimate Vault for your downloads. It embeds Radarr, Sonarr, Lidarr and Jackett coupled with RTorrent to build the ultimate download server.


## Installation

You must have Docker and docker-compose on your computer to make it work. 

- Copy the `docker-compose.yaml.dist` file into `docker-compose.yaml`
- Edit it so you can remove all unecessary stuff. For instance, don't keep 2 torrent clients.
- Simply execute `docker-compose up` to start building the application.

This will load all services with the default options. You will have to set everything up to make them work together.


## Nginx

To make them accessible from the outside, Nginx will work as a reversed proxy. 

Copy the default configuration file and replace all occurence of `%DOMAIN%` with your domain. 
```cp Nginx/vault.conf.dist Nginx/vault.conf```

Then, link this file to the nginx enabled sites.
```sudo ln -sr Nginx/vault.conf /etc/nginx/sites-enabled```
